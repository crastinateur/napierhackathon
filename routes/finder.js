var express = require('express');
var router = express.Router();
const Sequelize = require('sequelize');

const sequelize = new Sequelize('dbtest', 'root', 'admin', {
  host: 'localhost',
  dialect: 'mysql',

  pool: {
    max: 5,
    min: 0,
    idle: 10000
  }
});

sequelize.authenticate().then(() => {
    console.log('Connection has been established successfully.');
  }).catch(err => {
    console.error('Unable to connect to the database:', err);
});

const Amsterdam = sequelize.define('amsterdam', {
  pname: {
    type: Sequelize.STRING
  },
  age: {
    type: Sequelize.STRING
  }
}, {  freezeTableName: true});

/* GET finder page. */
router.get('/', function(req, res, next) {


	Amsterdam.findOne({ where: {room_id: 2818}, attributes: ['room_type'] }).then(amsterdam => {
    console.log(amsterdam.get('room_type'));
    ams = amsterdam.get('room_type');
    res.render('finder', { title: ams }); //pass back some stuff here.
  });

});

module.exports = router;
