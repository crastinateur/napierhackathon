var express = require('express');
var router = express.Router();
var Sequelize = require('sequelize');
var username;
const sequelize = new Sequelize('dbtest', 'root', 'admin', {
  host: 'localhost',
  dialect: 'mysql',

  pool: {
    max: 5,
    min: 0,
    idle: 10000
  }
});

sequelize.authenticate().then(() => {
    console.log('Connection has been established successfully.');
  }).catch(err => {
    console.error('Unable to connect to the database:', err);
});


const User = sequelize.define('tabletest', {
  pname: {
    type: Sequelize.STRING
  },
  age: {
    type: Sequelize.STRING
  }
});


/* GET home page. */
router.get('/', function(req, res, next) {

  User.findOne({ where: {pname: 'Gillan'}, attributes: ['pname'] }).then(user => {
    console.log(user.get('pname'));
    username = user.get('pname');
    res.render('index', { title: username });
  });
});

module.exports = router;
