poscenter = 0;
totalwidth= $("body").innerWidth();
defaultCenterColor = "rgba(215, 215, 215)";
defaultCenterFontColor = "rgb(25,25,25)";
center = $('#center');
function init(){//INITIALISATION
	//TITRE
	title = $('#title');
	Justify(title);
	title.css('display','inherit');
	title.click(function(){
		title.css('background-color',getRandomColor());
	});

	//center
	center.draggable({ axis: "x" });
	center.mousemove(function(){

		poscenter=center.position().left+0.5*totalwidth;
		update();
	});

	center.mousemove(function(){
		update();
	});
	center.mousedown(function(){
		center.css('transition','background-color 0.5s');
	});

	center.mouseup(function(){
		poscenter=$('#center').position().left+0.5*totalwidth;
		center.css('transition','1.5s');
		NeedReset = true;

		if(poscenter>0.55*totalwidth){
			center.css('left',2*totalwidth);
		}else if (poscenter<0.45*totalwidth) {
			center.css('left',-2*totalwidth);
		}else {
			center.css('left','0');
			NeedReset = false;
		}
		center.css('background-color',defaultCenterColor);
		if (NeedReset) {
			setTimeout(reset, 500);
		}
	});

}

//reset----------------------------------------------------------------------------------------------------------------------------------
function reset(){
	center.css('transition','0s');
	center.css('left','0');
	center.css('opacity','0');
	//NEW DATAs

	$('#title').css('background-color',getRandomColor());  //random color changing for each new place
	setTimeout(resetDisplay, 400);
}

function resetDisplay(){
	center.css('transition','1.5s');
	center.css('opacity','1');
}
//update---------------------------------------------------------------------------------------------------------------------------------
function update(){
	poscenter=$('#center').position().left+0.5*totalwidth;
	$('#position').html("x:"+poscenter+"|total width screen="+ $("body").innerWidth());
	if(poscenter>0.55*totalwidth){
		center.css('background-color','green');
		center.css('color','white');
	}else if (poscenter<0.45*totalwidth) {
		center.css('background-color','rgb(169, 31, 31)');
		center.css('color','white');
	}else {
		center.css('background-color',defaultCenterColor);
		center.css('color',defaultCenterFontColor);
	}
	setTimeout(update, 1000);
}

//OUTILS---------------------------------------------------------------------------------------------------------------------------------


function getRandomColor() {
	var letters = '0123456789ABCDEF';
	var color = '#';
	for (var i = 0; i < 6; i++) {
		color += letters[Math.floor(Math.random() * 16)];
	}
	return color;
}

function SplitText(node)
{

    var text = node.nodeValue.replace(/^\s*|\s(?=\s)|\s*$/g, "");

    for(var i = 0; i < text.length; i++)
    {
        var letter = document.createElement("span");
        letter.style.display = "inline-block";
        letter.style.position = "absolute";
        letter.appendChild(document.createTextNode(text.charAt(i)));
        node.parentNode.insertBefore(letter, node);

        var positionRatio = i / (text.length - 1);
        var textWidth = letter.clientWidth;

        var indent = 100 * positionRatio;
        var offset = -textWidth * positionRatio;
        letter.style.left = indent + "%";
        letter.style.marginLeft = offset + "px";

        //console.log("Letter ", text[i], ", Index ", i, ", Width ", textWidth, ", Indent ", indent, ", Offset ", offset);
    }

    node.parentNode.removeChild(node);
}

function Justify()
{

    var TEXT_NODE = 3;
    var elem = document.getElementById("title");
    elem = elem.firstChild;

    while(elem)
    {
        var nextElem = elem.nextSibling;

        if(elem.nodeType == TEXT_NODE)
            SplitText(elem);

        elem = nextElem;
    }
}
